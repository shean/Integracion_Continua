package com.ingeneo.ejb;

import javax.ejb.Local;


@Local
public interface LocalInterface {
    
    public String login(User user);
    public void save(String gusto,String herramienta,String correo);
}
