/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.ingeneo.bean;

import com.ingeneo.ejb.Ejb;
import com.ingeneo.ejb.LocalInterface;
import com.ingeneo.ejb.User;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.swing.JOptionPane;
import org.primefaces.context.RequestContext;
import javax.faces.application.FacesMessage;


/**
 *
 * @author Administrator
 */
@ManagedBean(name="myBean")
@RequestScoped
public class Bean implements Serializable{
    
    User user = new User();
    
    @EJB
    public LocalInterface myEjb;
    
    private String username;
    
    private String password;
    
    private String gusto;
    
    private String herramienta;
    
    private String correo;
    
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    public void addMessage1(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, summary,null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public String getGusto() {
        return gusto;
    }
    
    public void setGusto(String gusto) {
        this.gusto = gusto;
    }
    
    public String getHerramienta() {
        return herramienta;
    }
    
    public void setHerramienta(String herramienta) {
        this.herramienta = herramienta;
    }
    
    public String getCorreo() {
        return correo;
    }
    
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public Bean() {
        
    }
    
    public String login() {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean loggedIn = false;
        String save="";
        
        myEjb = new Ejb();
        
        user.setUsername(getUsername());
        user.setPassword(getPassword());
        save =  myEjb.login(user);
        
        if (save!=null) {
            
            loggedIn = true;
            addMessage("Welcome ",getUsername());
            return "form.xhtml";
        }
        else {
            loggedIn = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error en el Loggin", "Credenciales Invalidas");
   
        }
        FacesContext.getCurrentInstance().addMessage(null, message);
        
        return "index.xhtml";
    }
    
    
    public void save(){
        FacesMessage message = null;
        myEjb = new Ejb();
        
        user.setGusto(getGusto());
        user.setHerramienta(getHerramienta());
        user.setCorreo(getCorreo());
        
        myEjb.save(user.getGusto(),user.getHerramienta(),user.getCorreo());
    }
    
    
}
